package ParallelComputing;

import ActiveMQ.ActiveMQ;
import CSP.ProducerConsumerService;
import Serial.InsertionSort;
import ThreadLock.ParallelInsertionSort;

import java.text.DecimalFormat;

import javax.jms.JMSException;

public class MainDriver {

    private final static Utils utils = new Utils();
    private final static DecimalFormat df = new DecimalFormat("#.##");
    private final static int[] numbers = {1000, 10000, 25000};
    private final static int[] threads = {1, 2, 4, 6};
    private static double serialTime;

    public static void main(String[] args) throws JMSException {

        for (int i : numbers) {
            int[] unsortedArray = utils.unsortedArray(i);
            System.out.println("\n----------Array size: " + unsortedArray.length + "----------\n");
            serial(unsortedArray);

            System.out.println();
            for (int j : threads) { tl(unsortedArray, j); }

            System.out.println();
            for (int k : threads) { csp(unsortedArray, k); }

            //Needs activeMQ
            System.out.println();
            for (int l : threads) { activeMQ(unsortedArray, l); }
        }

    }

    public static double timesFaster(double time) {
        return ((serialTime / time) * 100) / 100;
    }

    public static void serial(int[] array) {
        int[] unsortedArray = array.clone();
        InsertionSort is = new InsertionSort();
        double startTime = System.currentTimeMillis();
        is.sort(unsortedArray);
        double endTime = System.currentTimeMillis();
        double timeElapsed = (endTime - startTime);
        serialTime = timeElapsed;
        System.out.println("Serial [Time: " + timeElapsed + " ms, Sorted: " + utils.isSorted(unsortedArray) + "]");
    }

    public static void tl(int[] array, int threads) {
        int[] unsortedArray = array.clone();
        ParallelInsertionSort par = new ParallelInsertionSort(unsortedArray, threads);
        double startTime = System.currentTimeMillis();
        par.sort();
        double endTime = System.currentTimeMillis();
        double timeElapsed = (endTime - startTime);
        System.out.println("TL [Threads: " + threads +
                ", Time: " + timeElapsed + " ms, " +
                "Sorted: " + utils.isSorted(unsortedArray) +
                ", TimesFaster: " + df.format(timesFaster(timeElapsed)) + "x ]");
    }

    public static void csp(int[] array, int threads) {
        int[] unsortedArray = array.clone();
        ProducerConsumerService csp = new ProducerConsumerService();
        long startTime = System.currentTimeMillis();
        csp.sort(unsortedArray, threads);
        long endTime = System.currentTimeMillis();
        long timeElapsed = (endTime - startTime);
        System.out.println("CSP [Threads: " + threads +
                ", Time: " + timeElapsed + " ms, " +
                "Sorted: " + utils.isSorted(unsortedArray) + "" +
                ", TimesFaster: " + df.format(timesFaster(timeElapsed)) + "x ]");
    }

    public static void activeMQ(int[] array, int threads) throws JMSException {
        int[] unsortedArray = array.clone();
        ActiveMQ activeMQ = new ActiveMQ();
        activeMQ.setDebugON(false);
        long startTime = System.currentTimeMillis();
        activeMQ.sort(unsortedArray, threads);
        long endTime = System.currentTimeMillis();
        long timeElapsed = (endTime - startTime);
        System.out.println("ActiveMQ CSP [Threads: " + threads +
                ", Time: " + timeElapsed + " ms, " +
                "Sorted: " + utils.isSorted(unsortedArray) + "" +
                ", TimesFaster: " + df.format(timesFaster(timeElapsed)) + "x ]");
    }

}
