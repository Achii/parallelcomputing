package ParallelComputing;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Achii on 17-5-2017.
 */
public class Utils {

	public int[] unsortedArray(int amount){
		int[] array = new int[amount];
		ThreadLocalRandom rnd = ThreadLocalRandom.current();
		for(int i=0; i < array.length; i++){
			array[i] = (1 + rnd.nextInt(amount));
		}
		return array;
	}

	// source: http://stackoverflow.com/questions/1519736/random-shuffling-of-an-array
	// Implementing Fisher�Yates shuffle
	public void shuffleArray(int[] ar) {
		// If running on Java 6 or older, use `new Random()` on RHS here
		ThreadLocalRandom rnd = ThreadLocalRandom.current();
		for (int i = ar.length - 1; i > 0; i--) {
			int index = rnd.nextInt(i + 1);
			// Simple swap
			int a = ar[index];
			ar[index] = ar[i];
			ar[i] = a;
		}
	}

	public int[] arrayListToArray(ArrayList<Integer> arrayList){
		//Convert arrayList to array
		int[] array= new int[arrayList.size()];
		for(int i=0; i < arrayList.size(); i++){
			array[i] = arrayList.get(i);
		}
		return array;
	}

	public void printArray(int[] anArray) {
		System.out.print("Array: ");
		for (int i=0; i< anArray.length; i++){
			System.out.print(anArray[i]+" ");
		}
		System.out.println();
	}


	public int[] fillArray(int amount) {
		int[] result = new int[amount];
		for (int i=0; i<amount; i++){
			result[i] = i;
		}
		return result;
	}

	public void addValue(int[] anArray, int value) {
		for (int i=0; i<anArray.length; i++){
			anArray[i] += value;
		}
	}

	public boolean isSorted(int[] a) {
		for (int i = 0; i < a.length - 1; i++) {
			if (a[i] > a[i + 1]) {
				return false; // It is proven that the array is not sorted.
			}
		}
		return true; // If this part has been reached, the array must be sorted.
	}
}
