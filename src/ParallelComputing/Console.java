package ParallelComputing;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Utils | Templates
 * and open the template in the editor.
 */

import ActiveMQ.ActiveMQ;
import CSP.ProducerConsumerService;
import ThreadLock.ParallelInsertionSort;
import Serial.InsertionSort;

import java.util.Scanner;

import javax.jms.JMSException;


/**
 * @author Ahmed Tyghri
 */
public class Console {

	private final static Utils ut = new Utils();
	private final static Scanner sc = new Scanner(System.in);

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) throws JMSException {

		System.out.println("Enter array size: ");
		int size = sc.nextInt();

		int[] unsortedArray = ut.unsortedArray(size);
		System.out.println("Created unsorted Array!");

		System.out.println("Choose: Serial=1, ThreadLock=2, CSP=3, ActiveMQ=4");
		int option = sc.nextInt();
		int amount;
		switch (option) {
			case 1:
				serial(unsortedArray);
				break;
			case 2:
				System.out.println("Enter threads amount: ");
				int threads = sc.nextInt();
				parallel(unsortedArray, threads);
				break;
			case 3:
				System.out.println("Enter Consumer amount: ");
				amount = sc.nextInt();
				csp(unsortedArray, amount);
				break;
			case 4:
				System.out.println("Enter Consumer amount: ");
				amount = sc.nextInt();
				activeMQ(unsortedArray, amount);
				break;
			default:
				System.out.println("Invalid option");
		}
		System.out.println("Is sorted: " + ut.isSorted(unsortedArray));
	}

	public static void serial(int[] array) {
		System.out.println("----------Serial Insertion Sort----------");
		InsertionSort is = new InsertionSort();
		long startTime = System.currentTimeMillis();
		is.sort(array);
		long endTime = System.currentTimeMillis();
		long timeElapsed = (endTime - startTime);
		System.out.println("Sorting time: " + timeElapsed + " ms");
	}

	public static void parallel(int[] array, int threads) {
		System.out.println("----------Parallel Insertion Sort----------");
		ParallelInsertionSort par = new ParallelInsertionSort(array, threads);
		long startTime = System.currentTimeMillis();
		par.sort();
		long endTime = System.currentTimeMillis();
		long timeElapsed = (endTime - startTime);
		System.out.println("ThreadLock: " + threads
				+ "\nSorting time: " + timeElapsed + " ms");
	}

	public static void csp(int[] array, int amount) {
		System.out.println("----------Producer and Consumer----------");
		ProducerConsumerService csp = new ProducerConsumerService();
		long startTime = System.currentTimeMillis();
		csp.sort(array, amount);
		long endTime = System.currentTimeMillis();
		long timeElapsed = (endTime - startTime);
		System.out.println("Consumers: " + amount
				+ "\nSorting time: " + timeElapsed + " ms");
	}

	public static void activeMQ(int[] array, int amount) throws JMSException {
		System.out.println("----------ActiveMQ----------");
		ActiveMQ activemq = new ActiveMQ();
		long startTime = System.currentTimeMillis();
		activemq.sort(array, amount);
		long endTime = System.currentTimeMillis();
		long timeElapsed = (endTime - startTime);
		System.out.println("Consumers: " + amount
				+ "\nSorting time: " + timeElapsed + " ms");
	}

}
