package CSP;

import ParallelComputing.Utils;
import Serial.InsertionSort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.BlockingQueue;

/**
 * Created by Ahmed Tyghri on 31-5-2017.
 */
public class Consumer extends Thread {

    private int id;
    private int low, high;
    private ArrayList<Integer> arrayList = new ArrayList<>();
    private InsertionSort insertion = new InsertionSort();
    private BlockingQueue<Integer> queue;
    private int[] conArray;
    private Utils ut = new Utils();

    public Consumer(int id, int low, int high, BlockingQueue<Integer> queue) {
        this.id = id;
        this.low = low;
        this.high = high;
        this.queue = queue;
    }

    @Override
    public void run() {


        for (Integer n : queue) {
            if (inRange(n)) {
                this.arrayList.add(n);
            }
        }

        conArray = new int[arrayList.size()];
        for(int i=0; i < arrayList.size(); i++){
            conArray[i] = arrayList.get(i);
        }

        insertion.sort(conArray);
//        System.out.println(toString());

    }

    public int[] getConArray() {
        return conArray;
    }

    public ArrayList<Integer> getArrayList() {
        return arrayList;
    }

    private boolean inRange(Integer number) {
        return number >= low && number <= high;
    }

    public void addNumber(Integer number) {
        if (inRange(number)) this.arrayList.add(number);
    }

    @Override
    public String toString() {
        String array = Arrays.toString(arrayList.toArray());
        return "Consumer #" + this.id +
                ", Range: " + this.low + " t/m " + this.high +
                ", Array size: " + this.arrayList.size() +
                "array: " + Arrays.toString(conArray);
    }
}
