package CSP;

import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by Ahmed Tyghri on 31-5-2017.
 */
public class ProducerConsumerService {

    private ArrayList<Consumer> consumers = new ArrayList<Consumer>();
    private BlockingQueue<Integer> queue;

    public void sort(int[] array, int amount) {

        this.queue = new ArrayBlockingQueue<>(array.length);

        //Producer
        Producer producer = new Producer(array, queue);

        //Create the amount consumers with their ranges
        int factor = (array.length / amount);
        for (int i = 0; i < amount; i++) {
            int low = (factor * i);
            int high = (i == amount - 1) ?  array.length : ((factor * (i + 1) - 1));
            Consumer consumer = new Consumer((i + 1), low, high, queue);
            consumers.add(consumer);
        }

        try {
            producer.start(); //start producer to make partitions
            producer.join(); //Wait until it is done
            //starting consumers to sort their partition
            for (Consumer consumer : consumers) {
                consumer.start();
            }
            //Wait until all consumer partitions are sorted
            for (Consumer consumer : consumers) {
                consumer.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        int count = 0;
        for(Consumer consumer: consumers){
           int[] conArray = consumer.getConArray();
           for(int i=0; i < conArray.length; i++){
               array[count++] = conArray[i];
           }
        }

    }




}
