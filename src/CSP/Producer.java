package CSP;

import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;

/**
 * Created by Ahmed Tyghri on 31-5-2017.
 */

public class Producer extends Thread {

    private int[] array;
    private ArrayList<Consumer> consumers;
    private BlockingQueue<Integer> queue;

    public Producer(int[] array, BlockingQueue<Integer> queue) {
        this.array = array;
        this.queue = queue;
    }

    @Override
    public void run() {

        for (int i = 0; i < array.length; i++) {
            Integer number = array[i];
            queue.offer(number);
        }

    }

}
