package ThreadLock;

/**
 * Created by treyjarrin on 17/05/2017.
 */
public class ParallelInsertionSort {

    private int[] array;
    private int chunks;
    private Thread[] threads;
    private boolean even;

    public ParallelInsertionSort(int[] array, int chunks) {
        this.array = array;
        this.chunks = chunks;
        threads = new Thread[chunks];
        int chunksize = (array.length - 2) / chunks;

        for (int i = 0; i < threads.length; i++) {
            if (i == 0) {
                threads[i] = new Thread(new InsertionLeftToRight(1, array));
            } else if (i == (threads.length - 1)) {
                threads[i] = new Thread(new InsertionRightToLeft(((i + 1) * chunksize), array));
            } else {
                if (isEven(i)) {
                    threads[i] = new Thread(new InsertionRightToLeft(((i + 1) * chunksize), array));
                } else {
                    threads[i] = new Thread(new InsertionLeftToRight(((i + 1) * chunksize), array));
                }
            }
        }
    }

    private boolean isEven(int i) {
        return ((i % 2) == 0);
    }

    public void sort() {
        for (Thread task : threads) task.start();
        join();
        sleep();
    }

    private void join() {
        for (Thread task : threads)
            try {
                task.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    }

    private void sleep() {
        for (Thread task : threads)
            task.interrupt();
    }
}
