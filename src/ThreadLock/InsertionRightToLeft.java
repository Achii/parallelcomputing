package ThreadLock;

/**
 * Created by treyjarrin on 17/05/2017.
 */
public class InsertionRightToLeft implements Runnable {
    private int startpos;
    private int[] array;

    public InsertionRightToLeft(int startpos, int[] array) {
        this.startpos = startpos;
        this.array = array;
    }

    @Override
    public void run() {
        int x;
        for (int i = startpos; i >= 0; i--) {
            x = array[i];
            while((i+1 <= array.length-1) && (x > array[i+1])){
                array[i] = array[i+1];
                i++;
            }
            array[i] = x;
        }
    }
}
