package ThreadLock;

/**
 * Created by treyjarrin on 17/05/2017.
 */
public class InsertionLeftToRight implements Runnable {

    private int startpos;
    private int[] array;

    public InsertionLeftToRight(int startpos, int[] array) {
        this.startpos = startpos;
        this.array = array;
    }

    @Override
    public void run() {
        int x;
        for (int i = startpos; i < array.length; i++) {
            x = array[i];
            while ((i - 1 >= 0) && (x < array[i - 1])) {
                array[i] = array[i - 1];
                i--;
            }
            array[i] = x;
        }
    }
}
