/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Serial;

import java.util.ArrayList;

/**
 *
 * @author Achii
 */
public class InsertionSort {

	public void sort(int[] tabel) {
		int x;
		for (int i = 1; i < tabel.length; i++) {
			x = tabel[i];
			while ((i - 1 >= 0) && (x < tabel[i - 1])) {
				tabel[i] = tabel[i - 1];
				i--;
			}
			tabel[i] = x;
		}
	}

	public void sort(ArrayList<Integer> arrayList) {
		int x;
		for (int i = 1; i < arrayList.size(); i++) {
			x = arrayList.get(i);
			while ((i - 1 >= 0) && (x < arrayList.get(i - 1))) {
				arrayList.set(i, arrayList.get(i - 1));
				i--;
			}
			arrayList.set(i, x);
		}
	}

}
