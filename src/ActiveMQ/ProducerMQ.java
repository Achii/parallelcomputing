package ActiveMQ;

import javax.jms.*;

/**
 * Created by Achii on 20-6-2017.
 */
public class ProducerMQ extends Thread{

    private int[] array;
    private int low;
    private int high;
    private String subject; // Queue Name
    private Session session;
    private boolean debugON = false;

    public ProducerMQ(int[] array, int low, int high, Session session){
        this.array = array;
        this.low = low;
        this.high = high;
        this.subject =  "Queue" +low + "-" + high;
        this.session = session;
    }

    @Override
    public void run() {
        try {
            sendQueue();
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }


    public void sendQueue() throws JMSException {
        Destination destination = session.createQueue(subject);
        MessageProducer producer = session.createProducer(destination);
        String str = "";
        for(int i=0; i < array.length; i++){
            int number = array[i];
            if(inRange(number)){
                str = str + number + " ";
            }
        }
        TextMessage message = session.createTextMessage(str);
        producer.send(message);
//		System.out.println("Sent Message '" + message.getText() + "'");
        if(debugON)System.out.println("Send queue: " + subject);
    }

    private boolean inRange(Integer number) {
        return number >= low && number <= high;
    }

    public void setDebugON(boolean debugON) {
        this.debugON = debugON;
    }
}
