package ActiveMQ;

import javax.jms.*;

import Serial.InsertionSort;

/**
 * Created by Achii on 20-6-2017.
 */
public class ConsumerMQ extends Thread {


	private String subject; // Queue Name
	private InsertionSort insertion = new InsertionSort();
	private int[] integers = null;
	private Session session;
	private boolean debugON = false;

	public ConsumerMQ(int low, int high, Session session){
		this.subject =  "Queue" +low + "-" + high;
		this.session = session;
	}


	@Override
	public void run() {
		try {
			receiveQueue();
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}


	public void receiveQueue() throws JMSException {
		Destination destination_fromQueue = session.createQueue(subject);
		MessageConsumer consumer = session.createConsumer(destination_fromQueue);
		Message message = consumer.receive();

		if (message instanceof TextMessage) {
			TextMessage textMessage = (TextMessage) message;
			String str = textMessage.getText();
			if(debugON)System.out.println("Received queue: " + subject);
			String[] integerStrings = str.split(" ");  //to store the string of numbers retrieved from the queue
			integers = new int[integerStrings.length];
			for (int i = 0; i < integers.length; i++) {
				integers[i] = Integer.parseInt(integerStrings[i]);
			}
		}
		insertion.sort(integers);
	}

	public String getSubject() {
		return subject;
	}

	public int[] getIntegers() {
		return integers;
	}

	public void setDebugON(boolean debugON) {
		this.debugON = debugON;
	}
}
