package ActiveMQ;

import java.util.ArrayList;

import javax.jms.*;

import org.apache.activemq.ActiveMQConnectionFactory;

/**
 * Created by Achii on 20-6-2017.
 */

public class ActiveMQ {

	private String url = "failover:(tcp://169.254.1.1:61616,localhost:8161)";
	private ArrayList<ProducerMQ> producers = new ArrayList<>();
	private ArrayList<ConsumerMQ> consumers = new ArrayList<>();
	private boolean debugON = false;

	public void sort(int[] array, int amount) throws JMSException {
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);
		Connection connection = connectionFactory.createConnection();
		connection.start();
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

		//send queues
		int factor = (array.length / amount);
		for (int i = 0; i < amount; i++) {
			int low = (factor * i);
			int high = (i == amount - 1) ?  array.length : ((factor * (i + 1) - 1));
			ProducerMQ producerMQ = new ProducerMQ(array, low, high, session);
			producerMQ.setDebugON(debugON);
			producers.add(producerMQ);
			ConsumerMQ consumerMQ = new ConsumerMQ(low, high, session);
			consumerMQ.setDebugON(debugON);
			consumers.add(consumerMQ);
		}

		try {
			//Produce queue for each range
			for (ProducerMQ producerMQ : producers) {
				producerMQ.start();
			}
			for (ProducerMQ producerMQ : producers) {
				producerMQ.join();
			}
			//Receive queue for each range and sort it
			for (ConsumerMQ consumerMQ : consumers) {
				consumerMQ.start();
			}
			for (ConsumerMQ consumerMQ : consumers) {
				consumerMQ.join();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		connection.close();

		//Merge arrays to one array
		int count = 0;
		for(ConsumerMQ consumerMQ: consumers){
			int[] queueArray = consumerMQ.getIntegers();
			for(int i=0; i < queueArray.length; i++){
				array[count++] = queueArray[i];
			}
		}

	}

	public void setDebugON(boolean debugON) {
		this.debugON = debugON;
	}
}
